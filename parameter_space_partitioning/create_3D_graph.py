import argparse
import os
import pandas as pd

def create_3D_graph():

    f1 = open(f'alarm-red-green-plots/3D/alarm_pla_graph.query', 'r')
    command = f1.read()
    os.system(f'{command} > alarm-red-green-plots/3D/alarm_solution_function.txt')        
    
    f2 = open(f'alarm-red-green-plots/3D/alarm_solution_function.txt', 'r')
    result = f2.read()
    #parse the result from storm
    begin_c = result.find('Result (initial states): ') + 25
    end_c = result.find('\n', begin_c)
    sol_funct_new = result[begin_c:end_c].strip()

    f3 = open(f'alarm-red-green-plots/3D/alarm_pla_graph_3D.tex', 'r')
    tex_file = f3.read()
    begin_s = tex_file.find('meta={z+0.6}] {') + 15
    end_s = tex_file.find(';', begin_s)
    sol_funct_old = tex_file[begin_s:end_s].strip()
    tex_file.replace(sol_funct_old, sol_funct_new)

    #write new content
    with open('alarm-red-green-plots/3D/alarm_pla_graph_3D.tex', 'w') as f:
        f.write(tex_file)

    #plot the results
    os.system(f'pdflatex --enable-write18 --extra-mem-top=100000000 --synctex=1 alarm-red-green-plots/3D/alarm_pla_graph_3D.tex')
    os.system('rm alarm_pla_graph_3D.aux')
    os.system('rm alarm_pla_graph_3D.log')
    os.system('rm alarm_pla_graph_3D.synctex.gz')

if __name__ == "__main__":
        parser = argparse.ArgumentParser(description='execute parameter partitioning ')
        args = parser.parse_args()
        create_3D_graph()

