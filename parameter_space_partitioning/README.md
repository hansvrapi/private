## make_pla_tablle.py
	- python script that gives a table containing information about parameter space partitioning for different coverage values as output.
	- Command: python3 make_table.py

## run_pla_experiments.py
	- python script that runs all PLA experiments and plots the results
	- Output: pla_plot.pdf
	- Command: python3 run_pla_experiments.py
	
## create_2D_graph.py
	- creates the 2D graph for alarm
	- Output: alarm_pla_graph_2D.pdf
	- Command: python3 create_2D_graph.py

## create_3D_graph.py
	- creates the 3D graph for alarm
	- Output: alarm_pla_graph_3D.pdf
	- Command: python3 create_3D_graph.py
	
## output
	- contains the output data for the specific threshhold values (e.g. 0.3 for hailfinder)
