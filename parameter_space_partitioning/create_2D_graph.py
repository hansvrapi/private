import argparse
import os
import pandas as pd

def create_2D_graph():

    f = open(f'alarm-red-green-plots/2D/alarm_pla_graph.query', 'r')
    command = f.read()
    #create alarm_pla_graph.txt file
    os.system(f'{command}')   

    #save result in alarm_pla_graph.tex file 
    os.system(f'python3 alarm-red-green-plots/2D/pl.py --file  alarm-red-green-plots/2D/alarm_pla_graph.txt > alarm-red-green-plots/2D/alarm_pla_graph_2D.tex')

    #create plot
    os.system(f'pdflatex --enable-write18 --extra-mem-top=100000000 --synctex=1 alarm-red-green-plots/2D/alarm_pla_graph_2D.tex')
    os.system('rm alarm_pla_graph_2D.aux')
    os.system('rm alarm_pla_graph_2D.log')
    os.system('rm alarm_pla_graph_2D.synctex.gz')


if __name__ == "__main__":
        parser = argparse.ArgumentParser(description='execute parameter partitioning ')
        args = parser.parse_args()
        create_2D_graph()
