FROM hansvrapi/experiments:1

WORKDIR /experiments

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \ 
&& sudo apt-get install -y -q && export PATH=$PATH:/experiments/storm/build/bin

RUN pip install jpype1 

RUN pip install pandas

RUN apt-get update -y && sudo apt-get install -y texlive-latex-base && sudo apt-get install -y texlive-fonts-recommended && sudo apt-get install -y texlive-latex-extra

RUN sudo apt-get install -y texlive-science

RUN apt install -y chromium-chromedriver

COPY ./ /EXPS

RUN pip install sympy #needed for plots

INSTALL Z3
