#build storm
#cd storm
#rm -r build
#mkdir build && cd build
#cmake ..	
#make

#change to feasibility-JAIR directory
#cd ../..
#g++ experiment.cpp -o experiment
#./experiment

pdflatex latex_source/pso_gd.tex
rm pso_gd.aux
rm pso_gd.log

pdflatex latex_source/qcqp_gd.tex
rm qcqp_gd.aux
rm qcqp_gd.log

pdflatex latex_source/hailfinder_feasibility.tex
rm hailfinder_feasibility.aux
rm hailfinder_feasibility.log

pdflatex latex_source/hepar2_feasibility.tex
rm hepar2_feasibility.aux
rm hepar2_feasibility.log

pdflatex latex_source/alarm_feasibility.tex
rm alarm_feasibility.aux
rm alarm_feasibility.log

pdflatex latex_source/sachs_feasibility.tex
rm sachs_feasibility.aux
rm sachs_feasibility.log
