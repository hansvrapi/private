Storm-pars 1.6.4 (dev)

Date: Thu May 12 10:46:35 2022
Command line arguments: --explicit-drn pso-qcqp-gd-benchmarks/alarm/drn_files/alarm_32.drn --prop 'Pmin=? [F("BP1"|"HRSAT1"|"HREKG1"|"EXPCO21"|"PRESS1"|"PAP1")]
' --find-extremum
Current working directory: /home/hans/Desktop/feasibility-JAIR

Time for model construction: 0.146s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1118
Transitions: 	3174
Reward Models:  none
State Labels: 	107 labels
   * deadlock -> 3 item(s)
   * PCWP0 -> 3 item(s)
   * LVEDVOLUME1 -> 1 item(s)
   * HISTORY1 -> 4 item(s)
   * HISTORY0 -> 4 item(s)
   * BP0 -> 4 item(s)
   * CO2 -> 12 item(s)
   * STROKEVOLUME2 -> 36 item(s)
   * STROKEVOLUME1 -> 36 item(s)
   * LVFAILURE1 -> 18 item(s)
   * LVFAILURE0 -> 18 item(s)
   * HYPOVOLEMIA1 -> 9 item(s)
   * HYPOVOLEMIA0 -> 9 item(s)
   * HRBP1 -> 9 item(s)
   * ERRLOWOUTPUT0 -> 9 item(s)
   * HRSAT2 -> 18 item(s)
   * HR2 -> 3 item(s)
   * BP2 -> 4 item(s)
   * CATECHOL0 -> 3 item(s)
   * INSUFFANESTH1 -> 27 item(s)
   * HRSAT1 -> 18 item(s)
   * CATECHOL1 -> 3 item(s)
   * INSUFFANESTH0 -> 27 item(s)
   * TPR2 -> 9 item(s)
   * TPR1 -> 9 item(s)
   * CVP1 -> 1 item(s)
   * HREKG2 -> 9 item(s)
   * PRESS1 -> 24 item(s)
   * PRESS3 -> 24 item(s)
   * PCWP2 -> 3 item(s)
   * PCWP1 -> 3 item(s)
   * CO1 -> 12 item(s)
   * VENTLUNG0 -> 3 item(s)
   * MINVOL0 -> 12 item(s)
   * MINVOL3 -> 12 item(s)
   * PAP0 -> 24 item(s)
   * VENTALV0 -> 8 item(s)
   * PRESS2 -> 24 item(s)
   * INTUBATION2 -> 8 item(s)
   * INTUBATION1 -> 8 item(s)
   * INTUBATION0 -> 8 item(s)
   * KINKEDTUBE1 -> 4 item(s)
   * init -> 1 item(s)
   * CVP0 -> 1 item(s)
   * EXPCO23 -> 24 item(s)
   * VENTLUNG1 -> 3 item(s)
   * HRSAT0 -> 18 item(s)
   * MINVOLSET0 -> 1 item(s)
   * LVEDVOLUME2 -> 1 item(s)
   * VENTTUBE1 -> 1 item(s)
   * STROKEVOLUME0 -> 36 item(s)
   * MINVOLSET1 -> 1 item(s)
   * HREKG1 -> 9 item(s)
   * TPR0 -> 9 item(s)
   * PAP1 -> 24 item(s)
   * VENTTUBE2 -> 1 item(s)
   * PRESS0 -> 24 item(s)
   * CVP2 -> 1 item(s)
   * BP1 -> 4 item(s)
   * HR0 -> 3 item(s)
   * VENTMACH0 -> 1 item(s)
   * HREKG0 -> 9 item(s)
   * ERRCAUTER0 -> 9 item(s)
   * ARTCO22 -> 32 item(s)
   * MINVOL1 -> 12 item(s)
   * ERRLOWOUTPUT1 -> 9 item(s)
   * VENTMACH2 -> 1 item(s)
   * VENTTUBE0 -> 1 item(s)
   * DISCONNECT0 -> 4 item(s)
   * PAP2 -> 24 item(s)
   * HR1 -> 3 item(s)
   * VENTMACH3 -> 1 item(s)
   * MINVOLSET2 -> 1 item(s)
   * HRBP2 -> 9 item(s)
   * VENTLUNG3 -> 3 item(s)
   * DISCONNECT1 -> 4 item(s)
   * LVEDVOLUME0 -> 1 item(s)
   * EXPCO21 -> 24 item(s)
   * FIO20 -> 24 item(s)
   * VENTLUNG2 -> 3 item(s)
   * SAO20 -> 3 item(s)
   * MINVOL2 -> 12 item(s)
   * ERRCAUTER1 -> 9 item(s)
   * SHUNT0 -> 12 item(s)
   * PULMEMBOLUS1 -> 12 item(s)
   * SHUNT1 -> 12 item(s)
   * ARTCO20 -> 32 item(s)
   * VENTALV1 -> 8 item(s)
   * VENTALV2 -> 8 item(s)
   * KINKEDTUBE0 -> 4 item(s)
   * PULMEMBOLUS0 -> 12 item(s)
   * VENTALV3 -> 8 item(s)
   * ARTCO21 -> 32 item(s)
   * VENTMACH1 -> 1 item(s)
   * EXPCO20 -> 24 item(s)
   * PVSAT1 -> 6 item(s)
   * VENTTUBE3 -> 1 item(s)
   * EXPCO22 -> 24 item(s)
   * FIO21 -> 24 item(s)
   * HRBP0 -> 9 item(s)
   * SAO22 -> 3 item(s)
   * CO0 -> 12 item(s)
   * PVSAT0 -> 6 item(s)
   * PVSAT2 -> 6 item(s)
   * SAO21 -> 3 item(s)
   * ANAPHYLAXIS0 -> 9 item(s)
   * ANAPHYLAXIS1 -> 9 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Time for model simplification: 0.056s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	181
Transitions: 	2375
Reward Models:  none
State Labels: 	108 labels
   * MINVOL2 -> 0 item(s)
   * MINVOL1 -> 0 item(s)
   * LVFAILURE1 -> 0 item(s)
   * LVFAILURE0 -> 0 item(s)
   * KINKEDTUBE0 -> 0 item(s)
   * PULMEMBOLUS0 -> 0 item(s)
   * VENTALV3 -> 0 item(s)
   * INTUBATION2 -> 0 item(s)
   * INSUFFANESTH1 -> 0 item(s)
   * HYPOVOLEMIA1 -> 0 item(s)
   * HYPOVOLEMIA0 -> 0 item(s)
   * INTUBATION1 -> 0 item(s)
   * INSUFFANESTH0 -> 0 item(s)
   * HRSAT1 -> 1 item(s)
   * CATECHOL1 -> 0 item(s)
   * HRSAT2 -> 18 item(s)
   * INTUBATION0 -> 0 item(s)
   * HRSAT0 -> 18 item(s)
   * MINVOLSET0 -> 0 item(s)
   * HREKG2 -> 0 item(s)
   * CVP1 -> 1 item(s)
   * PRESS1 -> 1 item(s)
   * LVEDVOLUME2 -> 1 item(s)
   * VENTTUBE1 -> 0 item(s)
   * HREKG1 -> 1 item(s)
   * PAP1 -> 1 item(s)
   * TPR0 -> 0 item(s)
   * HREKG0 -> 0 item(s)
   * ARTCO22 -> 24 item(s)
   * ERRCAUTER0 -> 9 item(s)
   * LVEDVOLUME1 -> 1 item(s)
   * HISTORY1 -> 1 item(s)
   * HRBP2 -> 0 item(s)
   * DISCONNECT1 -> 0 item(s)
   * VENTLUNG3 -> 0 item(s)
   * HR0 -> 0 item(s)
   * BP1 -> 1 item(s)
   * CVP2 -> 1 item(s)
   * VENTMACH0 -> 0 item(s)
   * CO0 -> 12 item(s)
   * PVSAT0 -> 0 item(s)
   * HR2 -> 0 item(s)
   * CO2 -> 12 item(s)
   * BP0 -> 1 item(s)
   * KINKEDTUBE1 -> 0 item(s)
   * EXPCO23 -> 0 item(s)
   * CVP0 -> 1 item(s)
   * CATECHOL0 -> 0 item(s)
   * BP2 -> 1 item(s)
   * FIO21 -> 0 item(s)
   * PRESS0 -> 0 item(s)
   * PRESS2 -> 0 item(s)
   * MINVOLSET1 -> 0 item(s)
   * STROKEVOLUME0 -> 0 item(s)
   * PRESS3 -> 0 item(s)
   * PULMEMBOLUS1 -> 0 item(s)
   * ERRLOWOUTPUT0 -> 0 item(s)
   * PAP2 -> 0 item(s)
   * PVSAT1 -> 0 item(s)
   * VENTMACH1 -> 0 item(s)
   * EXPCO20 -> 0 item(s)
   * ARTCO21 -> 32 item(s)
   * PVSAT2 -> 0 item(s)
   * SAO20 -> 0 item(s)
   * SAO22 -> 0 item(s)
   * HRBP0 -> 0 item(s)
   * init -> 1 item(s)
   * deadlock -> 1 item(s)
   * PAP0 -> 0 item(s)
   * VENTALV0 -> 0 item(s)
   * VENTTUBE2 -> 0 item(s)
   * VENTALV1 -> 0 item(s)
   * EXPCO22 -> 0 item(s)
   * VENTTUBE3 -> 0 item(s)
   * PCWP0 -> 1 item(s)
   * MINVOLSET2 -> 0 item(s)
   * VENTLUNG1 -> 0 item(s)
   * STROKEVOLUME1 -> 0 item(s)
   * VENTALV2 -> 0 item(s)
   * TPR2 -> 0 item(s)
   * STROKEVOLUME2 -> 0 item(s)
   * ANAPHYLAXIS0 -> 0 item(s)
   * TPR1 -> 0 item(s)
   * SHUNT1 -> 0 item(s)
   * SHUNT0 -> 0 item(s)
   * ERRCAUTER1 -> 9 item(s)
   * target -> 1 item(s)
   * ARTCO20 -> 32 item(s)
   * SAO21 -> 0 item(s)
   * VENTLUNG2 -> 0 item(s)
   * FIO20 -> 0 item(s)
   * LVEDVOLUME0 -> 1 item(s)
   * EXPCO21 -> 1 item(s)
   * VENTMACH2 -> 0 item(s)
   * VENTTUBE0 -> 0 item(s)
   * ERRLOWOUTPUT1 -> 0 item(s)
   * DISCONNECT0 -> 0 item(s)
   * HISTORY0 -> 1 item(s)
   * VENTMACH3 -> 0 item(s)
   * HR1 -> 0 item(s)
   * PCWP2 -> 1 item(s)
   * VENTLUNG0 -> 0 item(s)
   * CO1 -> 12 item(s)
   * PCWP1 -> 1 item(s)
   * HRBP1 -> 0 item(s)
   * MINVOL3 -> 0 item(s)
   * MINVOL0 -> 0 item(s)
   * ANAPHYLAXIS1 -> 0 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Time for model preprocessing: 0.056s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	181
Transitions: 	2375
Reward Models:  none
State Labels: 	108 labels
   * MINVOL2 -> 0 item(s)
   * MINVOL1 -> 0 item(s)
   * LVFAILURE1 -> 0 item(s)
   * LVFAILURE0 -> 0 item(s)
   * KINKEDTUBE0 -> 0 item(s)
   * PULMEMBOLUS0 -> 0 item(s)
   * VENTALV3 -> 0 item(s)
   * INTUBATION2 -> 0 item(s)
   * INSUFFANESTH1 -> 0 item(s)
   * HYPOVOLEMIA1 -> 0 item(s)
   * HYPOVOLEMIA0 -> 0 item(s)
   * INTUBATION1 -> 0 item(s)
   * INSUFFANESTH0 -> 0 item(s)
   * HRSAT1 -> 1 item(s)
   * CATECHOL1 -> 0 item(s)
   * HRSAT2 -> 18 item(s)
   * INTUBATION0 -> 0 item(s)
   * HRSAT0 -> 18 item(s)
   * MINVOLSET0 -> 0 item(s)
   * HREKG2 -> 0 item(s)
   * CVP1 -> 1 item(s)
   * PRESS1 -> 1 item(s)
   * LVEDVOLUME2 -> 1 item(s)
   * VENTTUBE1 -> 0 item(s)
   * HREKG1 -> 1 item(s)
   * PAP1 -> 1 item(s)
   * TPR0 -> 0 item(s)
   * HREKG0 -> 0 item(s)
   * ARTCO22 -> 24 item(s)
   * ERRCAUTER0 -> 9 item(s)
   * LVEDVOLUME1 -> 1 item(s)
   * HISTORY1 -> 1 item(s)
   * HRBP2 -> 0 item(s)
   * DISCONNECT1 -> 0 item(s)
   * VENTLUNG3 -> 0 item(s)
   * HR0 -> 0 item(s)
   * BP1 -> 1 item(s)
   * CVP2 -> 1 item(s)
   * VENTMACH0 -> 0 item(s)
   * CO0 -> 12 item(s)
   * PVSAT0 -> 0 item(s)
   * HR2 -> 0 item(s)
   * CO2 -> 12 item(s)
   * BP0 -> 1 item(s)
   * KINKEDTUBE1 -> 0 item(s)
   * EXPCO23 -> 0 item(s)
   * CVP0 -> 1 item(s)
   * CATECHOL0 -> 0 item(s)
   * BP2 -> 1 item(s)
   * FIO21 -> 0 item(s)
   * PRESS0 -> 0 item(s)
   * PRESS2 -> 0 item(s)
   * MINVOLSET1 -> 0 item(s)
   * STROKEVOLUME0 -> 0 item(s)
   * PRESS3 -> 0 item(s)
   * PULMEMBOLUS1 -> 0 item(s)
   * ERRLOWOUTPUT0 -> 0 item(s)
   * PAP2 -> 0 item(s)
   * PVSAT1 -> 0 item(s)
   * VENTMACH1 -> 0 item(s)
   * EXPCO20 -> 0 item(s)
   * ARTCO21 -> 32 item(s)
   * PVSAT2 -> 0 item(s)
   * SAO20 -> 0 item(s)
   * SAO22 -> 0 item(s)
   * HRBP0 -> 0 item(s)
   * init -> 1 item(s)
   * deadlock -> 1 item(s)
   * PAP0 -> 0 item(s)
   * VENTALV0 -> 0 item(s)
   * VENTTUBE2 -> 0 item(s)
   * VENTALV1 -> 0 item(s)
   * EXPCO22 -> 0 item(s)
   * VENTTUBE3 -> 0 item(s)
   * PCWP0 -> 1 item(s)
   * MINVOLSET2 -> 0 item(s)
   * VENTLUNG1 -> 0 item(s)
   * STROKEVOLUME1 -> 0 item(s)
   * VENTALV2 -> 0 item(s)
   * TPR2 -> 0 item(s)
   * STROKEVOLUME2 -> 0 item(s)
   * ANAPHYLAXIS0 -> 0 item(s)
   * TPR1 -> 0 item(s)
   * SHUNT1 -> 0 item(s)
   * SHUNT0 -> 0 item(s)
   * ERRCAUTER1 -> 9 item(s)
   * target -> 1 item(s)
   * ARTCO20 -> 32 item(s)
   * SAO21 -> 0 item(s)
   * VENTLUNG2 -> 0 item(s)
   * FIO20 -> 0 item(s)
   * LVEDVOLUME0 -> 1 item(s)
   * EXPCO21 -> 1 item(s)
   * VENTMACH2 -> 0 item(s)
   * VENTTUBE0 -> 0 item(s)
   * ERRLOWOUTPUT1 -> 0 item(s)
   * DISCONNECT0 -> 0 item(s)
   * HISTORY0 -> 1 item(s)
   * VENTMACH3 -> 0 item(s)
   * HR1 -> 0 item(s)
   * PCWP2 -> 1 item(s)
   * VENTLUNG0 -> 0 item(s)
   * CO1 -> 12 item(s)
   * PCWP1 -> 1 item(s)
   * HRBP1 -> 0 item(s)
   * MINVOL3 -> 0 item(s)
   * MINVOL0 -> 0 item(s)
   * ANAPHYLAXIS1 -> 0 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 
Parameters: p21 p25 p29 p22 p26 p30 p23 p27 p31 p24 p28 p9 p12 p10 p13 p11 p14 p15 p18 p16 p19 p17 p20 p0 p3 p6 p1 p4 p7 p2 p5 p8 
Finding an extremum using Gradient Descent
Found value 0.920108197 at instantiation 
p21=4722366482869645/4722366482869645213696,p25=4722366482869645/4722366482869645213696,p29=4722366482869645/4722366482869645213696,p22=4722366482869645/4722366482869645213696,p26=4722366482869645/4722366482869645213696,p30=4722366482869645/4722366482869645213696,p23=4722366482869645/4722366482869645213696,p27=4722366482869645/4722366482869645213696,p31=4722366482869645/4722366482869645213696,p24=4722366482869645/4722366482869645213696,p28=4722366482869645/4722366482869645213696,p9=4722366482869645/4722366482869645213696,p12=4722366482869645/4722366482869645213696,p10=4722366482869645/4722366482869645213696,p13=4722366482869645/4722366482869645213696,p11=4722366482869645/4722366482869645213696,p14=4722366482869645/4722366482869645213696,p15=4722366482869645/4722366482869645213696,p18=4722366482869645/4722366482869645213696,p16=4722366482869645/4722366482869645213696,p19=4722366482869645/4722366482869645213696,p17=4722366482869645/4722366482869645213696,p20=4722366482869645/4722366482869645213696,p0=4722366482869645/4722366482869645213696,p3=4722366482869645/4722366482869645213696,p6=4722366482869645/4722366482869645213696,p1=4722366482869645/4722366482869645213696,p4=4722366482869645/4722366482869645213696,p7=4722366482869645/4722366482869645213696,p2=4722366482869645/4722366482869645213696,p5=4722366482869645/4722366482869645213696,p8=4722366482869645/4722366482869645213696
Finished in 0.439s
