#include <iostream>
#include <fstream> //For file operation
#include <regex>  // For regular expression

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>
#include <array>


//#include "matplotlib-cpp/matplotlibcpp.h"
//namespace plt = matplotlibcpp;

std::string benchmarks_directory = "pso-qcqp-gd-benchmarks/";
std::string csvfiles_directory = "csv-files/";
std::string prophesy_directory = "../../prophesy/";
std::string storm_gd_directory = "storm/build/bin/";
double timeout = 15; /* minutes*/
enum Experiment { pso, qcqp, gd,gdex };



std::string readFile(std::string filePath, std::string defaultContent){
    std::ifstream ifs(filePath);
     std::string content( (std::istreambuf_iterator<char>(ifs) ),
                          (std::istreambuf_iterator<char>()    ) );
    return content;
}

void writeFile(std::string filePath, std::string fileContent){
    std::ofstream outputFile;
    outputFile.open(filePath);
    outputFile << fileContent;
}

std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
       throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

std::string getPSOTime(std::string textresult, std::string infoKey){
    std::istringstream f(textresult);
    std::string line;
    bool isTimeOut = true;
    while (std::getline(f, line)) {
        if(line.find(infoKey) != std::string::npos){
            isTimeOut = false;
            break;
        }
    }
    if(isTimeOut)
        return "10000"; /*TIMEOUT*/
    std::vector<std::string> infolinevector;
    std::istringstream iss(line);
    for(std::string s; iss >> line; )
        infolinevector.push_back(line);
    std::string info = infolinevector.at(3);
    info.erase(std::prev(info.end()));
    return info;
}

std::string getQCQPTime(std::string result, std::string infoKey){
    std::istringstream f(result);
    std::string line;
    bool isTimeOut = true;
    while (std::getline(f, line)) {
        if(line.find(infoKey) != std::string::npos){
            isTimeOut = false;
            std::string delimiter = "=";
            line.erase(0, line.find(delimiter) + delimiter.length());
            return line;
        }
    }
    return "5000"; /*TIMEOUT - two times will be summed up, so we took the half as the extra tick number*/
}


std::string getGDTime(std::string textresult, std::string infoKey){
    std::istringstream f(textresult);
    std::string line;
    bool isTimeOut = true;
    while (std::getline(f, line)) {
        if(line.find(infoKey) != std::string::npos){
            isTimeOut = false;
            break;
        }
    }
    if(isTimeOut)
        return "10000"; /*TIMEOUT*/
    std::vector<std::string> infolinevector;
    std::istringstream iss(line);
    for(std::string s; iss >> line; )
        infolinevector.push_back(line);
    std::string info = infolinevector.at(2);
    info.erase(std::prev(info.end()));
    return info;
}

std::string getNumberOfRealParams(std::string filePath, std::string addedParamsNum){
    std::string command = "python3 number_of_params.py " + filePath + " " + addedParamsNum;
    const char *cmnd = command.c_str();
    std::string str = exec(cmnd);
    str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
    return str;
}

std::string makePSOCommand(std::string networkName, std::string instance, std::string belowOrAbove, double threshhold){
    std::string pso_command = "";
    std::string prophesy_feasibility_command = "";
    prophesy_feasibility_command += "timeout " + std::to_string(timeout) + "m python3 " + "/opt/prophesy/scripts/" + "parameter_synthesis.py load-problem " + benchmarks_directory    + networkName + "/drn_files/" + networkName + "_" + instance + ".drn " + benchmarks_directory + networkName + "/" + networkName + ".pctl set-threshold " + std::to_string(threshhold) + " find-feasible-instantiation " + belowOrAbove + " ";
    pso_command += prophesy_feasibility_command + "pso";
    return pso_command;
}

std::string makeQCQPCommand(std::string networkName, std::string instance, std::string belowOrAbove, double threshhold){
    std::string qcqp_command = "";
    std::string prophesy_feasibility_command = "";
    prophesy_feasibility_command += "timeout " + std::to_string(timeout) + "m python3 " + "/opt/prophesy/scripts/" + "parameter_synthesis.py load-problem " + benchmarks_directory   + networkName +  "/drn_files/" + networkName + "_" + instance + ".drn " + benchmarks_directory + networkName + "/" + networkName + ".pctl set-threshold " + std::to_string(threshhold) + " find-feasible-instantiation " + belowOrAbove + " ";
    qcqp_command += prophesy_feasibility_command + "qcqp";
    std::cout << qcqp_command << "\n";
    return qcqp_command;
}

std::string makeGDCommands(std::string networkName, std::string instance, std::string belowOrAbove, double threshhold, std::string PCTLFormulae, double leaningRate){
    std::string gd_command = "";
    std::string PmaxPmin = "";
    if(belowOrAbove == "below"){
        belowOrAbove = "<=";
        PmaxPmin = "Pmin";
    }
    if(belowOrAbove == "above"){
        belowOrAbove = ">=";
        PmaxPmin = "Pmax";
    }
    gd_command += "timeout " + std::to_string(timeout) + "m " + storm_gd_directory + "storm-pars --explicit-drn " + benchmarks_directory + networkName + "/drn_files/" + networkName + "_" + instance + ".drn --prop \"" + PmaxPmin + belowOrAbove + std::to_string(threshhold) + " " + PCTLFormulae + "\" --find-feasible";
    std::cout << gd_command << "\n";
    return gd_command;
}

std::string makeGDCommandsExtremum(std::string networkName, std::string instance, std::string belowOrAbove, std::string PCTLFormulae, double leaningRate){
    std::string gd_command = "";
    std::string PmaxPmin = "";
    if(belowOrAbove == "below"){
        belowOrAbove = "<=";
        PmaxPmin = "Pmin";
    }
    if(belowOrAbove == "above"){
        belowOrAbove = ">=";
        PmaxPmin = "Pmax";
    }
    gd_command += "timeout " + std::to_string(timeout) + "m " + storm_gd_directory + "storm-pars --explicit-drn " + benchmarks_directory + networkName + "/drn_files/" + networkName + "_" + instance + ".drn --prop \"" + PmaxPmin + "=? " + PCTLFormulae + "\" --find-extremum";
    std::cout << gd_command << "\n";
    return gd_command;
}

void runASingleExperiment(std::string networkName, std::string instance, std::string belowOrAbove, double threshhold, std::string PCTLFormulae, double leaningRate, Experiment e){
    std::string command = "";
    std::string result;
    switch(e)
    {
        case pso  : command = makePSOCommand(networkName, instance, belowOrAbove, threshhold);  break;
        case qcqp: command = makeQCQPCommand(networkName, instance, belowOrAbove, threshhold); break;
        case gd : command = makeGDCommands(networkName, instance, belowOrAbove, threshhold, PCTLFormulae, leaningRate);  break;
        case gdex : command = makeGDCommandsExtremum(networkName, instance, belowOrAbove, PCTLFormulae, leaningRate);  break;
    }
    const char *cmnd = command.c_str();
    try{
        result = exec(cmnd);
    }
    catch(const std::exception& e){
        result = -1;
    }
    
    std::string filePathPrefix = benchmarks_directory + networkName + "/" + networkName + "_" + instance + "_" + std::to_string(threshhold) + "_" + belowOrAbove;
    switch(e)
    {
        case pso  : writeFile(filePathPrefix + "_pso.txt", result);  break;
        case qcqp  : writeFile(filePathPrefix + "_qcqp.txt", result);  break;
        case gd  : writeFile(filePathPrefix + "_gd.txt", result);  break;
        case gdex : writeFile(filePathPrefix + "_gd_extremum.txt", result);  break;
    }
}


void runFeasibilityExperimentsForOneNetwork(std::string network, double thresholds[], std::string instances[], std::string belowOrAbove[], int numberOfInstances){
        for(int i = 0; i < numberOfInstances; i++){
         std::string PCTLFormulae = readFile(benchmarks_directory + network + "/" + network + ".pctl", "P=? [F(true)]");
         std::string gd_format_PCTLFormulae = readFile(benchmarks_directory + network + "/" + network + "_gd_format.pctl", "P=? [F(true)]");
         //runASingleExperiment(network, instances[i], belowOrAbove[i], thresholds[i], PCTLFormulae, 0.1, pso);
         //runASingleExperiment(network, instances[i], belowOrAbove[i], thresholds[i], PCTLFormulae, 0.1, qcqp);
         runASingleExperiment(network, instances[i], belowOrAbove[i], thresholds[i], gd_format_PCTLFormulae, 0.1, gd);
         runASingleExperiment(network, instances[i], belowOrAbove[i], thresholds[i], gd_format_PCTLFormulae, 0.1, gdex);
    }
}


void runFeasibilityExperiments(){
    
    std::string network = "hepar2";
    double thresholds_hepar[10] = {0.9999,0.9995,0.9992,0.9990,0.9989,0.9988,0.9987,0.9986,0.1000,0.0500}; /* relaxed thresholds to be fair on qcqp and pso*/
    std::string instances_hepar[10] = {"1", "2", "4", "8","16","32","64","128","256","512"};
    std::string belowOrAbove_hepar[10] = {"below","below","below","below","below","below","below","below","below","below"};
    runFeasibilityExperimentsForOneNetwork(network, thresholds_hepar, instances_hepar, belowOrAbove_hepar, 10);
    
    
    network = "win95pts";
    std::string instances_w[9] = {"1", "2", "4", "8","32","64","128","256","512"};
    double thresholds_w[9] = {0.9998,0.9996,0.9980,0.9975,0.9721,0.9000,0.1000,0.0500,0.0200};
    std::string belowOrAbove_w[9] = {"below","below","below","below","below","below","below","below","below"};
    runFeasibilityExperimentsForOneNetwork(network, thresholds_w, instances_w, belowOrAbove_w, 9);


    network = "alarm";
    std::string instances_a[9] = {"1", "2", "4", "8","16","32","64","128","256"};
    double thresholds_a[9] = {0.9920,0.9917,0.9915,0.9900,0.9892,0.9210,0.1000,0.0500,0.0200};
    std::string belowOrAbove_a[9] = {"below","below","below","below","below","below","below","below","below"};
    runFeasibilityExperimentsForOneNetwork(network, thresholds_a, instances_a, belowOrAbove_a, 9);

    network = "sachs";
    std::string instances_s[8] = {"1", "2", "4", "8","16","32","64","128"};
    double thresholds_s[8] = {0.66,0.65,0.64,0.55,0.25,0.1,0.05,0.02};
    std::string belowOrAbove_s[8] = {"below","below","below","below","below","below","below","below"};
    runFeasibilityExperimentsForOneNetwork(network, thresholds_s, instances_s, belowOrAbove_s, 8);

    network = "hailfinder";
    std::string instances_h[7] = {"32", "64", "128", "256","512","1024","2048"};
    double thresholds_h[7] = {0.600000,0.600000,0.600000,0.600000,0.600000,0.600000,0.600000};
    std::string belowOrAbove_h[7] = {"below","below","below","below","below","below","below"};
    runFeasibilityExperimentsForOneNetwork(network, thresholds_h, instances_h, belowOrAbove_h, 7);
    
}


std::string parseFeasibilityExperimentsForOneNetwork(std::string networkName, double thresholds[], std::string instances[], std::string belowOrAbove[], int numberOfInstances){
        std::string csvcontent;
        std::string filePathPrefix = "";
        for(int i = 0; i < numberOfInstances; i++){
            filePathPrefix = benchmarks_directory + networkName + "/"  + networkName + "_" + instances[i] + "_" + std::to_string(thresholds[i]) + "_" + belowOrAbove[i];
            std::string result = readFile(filePathPrefix + "_pso.txt","");
            std::string psotime = getPSOTime(result, "This procedure took");
            
            result = readFile(filePathPrefix + "_qcqp.txt","");
            std::string qcqptime = std::to_string(std::stod((getQCQPTime(result, "solver time"))) + std::stod(getQCQPTime(result, "encoding time")));

            result = readFile(filePathPrefix + "_gd.txt","");
            std::string gdtime = getGDTime(result, "Finished in");
            
            std::string realparamnum = getNumberOfRealParams(filePathPrefix + "_gd.txt ", instances[i]);

            /* the new line on csvfile*/
        csvcontent += networkName + "," + instances[i] + "," + realparamnum + "," + std::to_string(thresholds[i])  + "," + gdtime + "," + psotime + "," + qcqptime + "," + networkName + "\n";
    }
    return csvcontent;
}


std::string parseFeasibilityExperiments(){
    std::string first_line = "networkname,networkinstance,realparamnum,threshhold,gdtime,psotime,qcqptime,scatterclass\n";
    std::string csvcontent = first_line;
    std::string parsecontent = "";

    std::string network = "hepar2";
    double thresholds_hepar[10] = {0.9999,0.9995,0.9992,0.9990,0.9989,0.9988,0.9987,0.9986,0.1000,0.0500};
    std::string instances_hepar[10] = {"1", "2", "4", "8","16","32","64","128","256","512"};
    std::string belowOrAbove_hepar[10] = {"below","below","below","below","below","below","below","below","below","below"};
    parsecontent = parseFeasibilityExperimentsForOneNetwork(network, thresholds_hepar, instances_hepar, belowOrAbove_hepar, 10);
    writeFile("csv_files/hepar2-feasibility-result.csv", first_line + parsecontent);
    csvcontent += parsecontent;

    
    network = "win95pts";
    std::string instances_w[9] = {"1", "2", "4", "8","32","64","128","256","512"};
    double thresholds_w[9] = {0.9998,0.9996,0.9980,0.9975,0.9721,0.9000,0.1000,0.0500,0.0200};
    std::string belowOrAbove_w[9] = {"below","below","below","below","below","below","below","below","below"};
    parsecontent = parseFeasibilityExperimentsForOneNetwork(network, thresholds_w, instances_w, belowOrAbove_w, 9);
    writeFile("csv_files/win95pts-feasibility-result.csv", first_line + parsecontent);
    csvcontent += parsecontent;


    network = "alarm";
    std::string instances_a[9] = {"1", "2", "4", "8","16","32","64","128","256"};
    double thresholds_a[9] = {0.9920,0.9917,0.9915,0.9900,0.9892,0.9210,0.1000,0.0500,0.0200};
    std::string belowOrAbove_a[9] = {"below","below","below","below","below","below","below","below","below"};
    parsecontent = parseFeasibilityExperimentsForOneNetwork(network, thresholds_a, instances_a, belowOrAbove_a, 9);
    writeFile("csv_files/alarm-feasibility-result.csv", first_line + parsecontent);
    csvcontent += parsecontent;

    network = "sachs";
    std::string instances_s[8] = {"1", "2", "4", "8","16","32","64","128"};
    double thresholds_s[8] = {0.66,0.65,0.64,0.55,0.25,0.1,0.05,0.02};
    std::string belowOrAbove_s[8] = {"below","below","below","below","below","below","below","below"};
    parsecontent = parseFeasibilityExperimentsForOneNetwork(network, thresholds_s, instances_s, belowOrAbove_s, 8); 
    writeFile("csv_files/sachs-feasibility-result.csv", first_line + parsecontent);
    csvcontent += parsecontent;

    network = "hailfinder";
    std::string instances_h[7] = {"32", "64", "128", "256","512","1024","2048"};
    double thresholds_h[7] = {0.600000,0.600000,0.600000,0.600000,0.600000,0.600000,0.600000};
    std::string belowOrAbove_h[7] = {"below","below","below","below","below","below","below"};
    parsecontent = parseFeasibilityExperimentsForOneNetwork(network, thresholds_h, instances_h, belowOrAbove_h, 7); 
    writeFile("csv_files/hailfinder-feasibility-result.csv", first_line + parsecontent);
    csvcontent += parsecontent;

    
    return csvcontent;
}


int main() {
    //runFeasibilityExperiments();
    std::string csvcontent = parseFeasibilityExperiments();
    writeFile("csv_files/feasibility-results.csv", csvcontent);
    return 0;
}

