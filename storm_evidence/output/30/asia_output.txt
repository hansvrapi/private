Storm 1.6.4

Date: Sun May 15 15:57:00 2022
Command line arguments: --jani jani_files/30/asia.jani --prop 'P=? [G(bronc=0 | bronc=-1)]' --engine dd
Current working directory: /home/hans/Desktop/private/storm_evidence

Time for model input parsing: 0.001s.

 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
Time for model construction: 0.084s.

-------------------------------------------------------------- 
Model type: 	DTMC (symbolic)
States: 	12 (63 nodes)
Transitions: 	20 (311 nodes)
Reward Models:  none
Variables: 	rows: 7 meta variables (15 DD variables), columns: 7 meta variables (15 DD variables)
Labels: 	2
   * deadlock -> 2 state(s) (17 nodes)
   * init -> 1 state(s) (16 nodes)
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((bronc = 0) | (bronc = -(1)))] ...
Result (for initial states): 0.6000256173
Time for model checking: 0.641s.
