Storm 1.6.4

Date: Sun May 15 16:31:57 2022
Command line arguments: --jani jani_files/30/hepar2.jani --prop 'P=? [G(irregular_liver=0 | irregular_liver=-1)]' --engine dd
Current working directory: /home/hans/Desktop/private/storm_evidence

Time for model input parsing: 0.057s.

Time for model construction: 6.545s.

-------------------------------------------------------------- 
Model type: 	DTMC (symbolic)
States: 	54 (278 nodes)
Transitions: 	113 (9079 nodes)
Reward Models:  none
Variables: 	rows: 39 meta variables (85 DD variables), columns: 39 meta variables (85 DD variables)
Labels: 	2
   * deadlock -> 3 state(s) (86 nodes)
   * init -> 1 state(s) (86 nodes)
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((irregular_liver = 0) | (irregular_liver = -(1)))] ...
Result (for initial states): 1
Time for model checking: 0.001s.
