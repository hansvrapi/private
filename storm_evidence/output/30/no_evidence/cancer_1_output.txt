Storm 1.6.4

Date: Sun May 15 15:57:41 2022
Command line arguments: --jani jani_files/no_evidence/cancer.jani --prop 'P=? [F(Cancer=1|Xray=1|Dyspnoea=1)]' --engine dd
Current working directory: /home/hans/Desktop/private/storm_evidence

Time for model input parsing: 0.001s.

Time for model construction: 0.072s.

-------------------------------------------------------------- 
Model type: 	DTMC (symbolic)
States: 	19 (50 nodes)
Transitions: 	30 (330 nodes)
Reward Models:  none
Variables: 	rows: 6 meta variables (13 DD variables), columns: 6 meta variables (13 DD variables)
Labels: 	2
   * deadlock -> 2 state(s) (15 nodes)
   * init -> 1 state(s) (14 nodes)
-------------------------------------------------------------- 

Model checking property "1": P=? [F (((Cancer = 1) | (Xray = 1)) | (Dyspnoea = 1))] ...
Result (for initial states): 0.99319645
Time for model checking: 0.023s.
