Storm 1.6.4

Date: Sun May 15 15:57:11 2022
Command line arguments: --jani jani_files/10/alarm.jani --prop 'P=? [G(BP=0 | BP=-1)]' --engine dd
Current working directory: /home/hans/Desktop/private/storm_evidence

Time for model input parsing: 0.009s.

 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
Time for model construction: 2.244s.

-------------------------------------------------------------- 
Model type: 	DTMC (symbolic)
States: 	10 (166 nodes)
Transitions: 	25 (763 nodes)
Reward Models:  none
Variables: 	rows: 27 meta variables (61 DD variables), columns: 27 meta variables (61 DD variables)
Labels: 	2
   * deadlock -> 2 state(s) (63 nodes)
   * init -> 1 state(s) (62 nodes)
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((BP = 0) | (BP = -(1)))] ...
Result (for initial states): 1
Time for model checking: 0.000s.
