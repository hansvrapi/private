Storm 1.6.4

Date: Sun May 15 15:56:59 2022
Command line arguments: --jani jani_files/no_evidence/asia.jani --prop 'P=? [F(either=1|bronc=1)]' --engine dd
Current working directory: /home/hans/Desktop/private/storm_evidence

Time for model input parsing: 0.001s.

 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
 WARN (DdJaniModelBuilder.cpp:1522): Destination does not have any effect.
Time for model construction: 0.167s.

-------------------------------------------------------------- 
Model type: 	DTMC (symbolic)
States: 	27 (103 nodes)
Transitions: 	41 (549 nodes)
Reward Models:  none
Variables: 	rows: 9 meta variables (20 DD variables), columns: 9 meta variables (20 DD variables)
Labels: 	2
   * deadlock -> 2 state(s) (21 nodes)
   * init -> 1 state(s) (21 nodes)
-------------------------------------------------------------- 

Model checking property "1": P=? [F ((either = 1) | (bronc = 1))] ...
Result (for initial states): 0.9641476
Time for model checking: 0.039s.
