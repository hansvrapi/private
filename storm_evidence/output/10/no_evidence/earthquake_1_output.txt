Storm 1.6.4

Date: Sun May 15 15:57:58 2022
Command line arguments: --jani jani_files/no_evidence/earthquake.jani --prop 'P=? [F(JohnCalls=1|MaryCalls=1)]' --engine dd
Current working directory: /home/hans/Desktop/private/storm_evidence

Time for model input parsing: 0.000s.

Time for model construction: 0.075s.

-------------------------------------------------------------- 
Model type: 	DTMC (symbolic)
States: 	15 (48 nodes)
Transitions: 	26 (333 nodes)
Reward Models:  none
Variables: 	rows: 6 meta variables (13 DD variables), columns: 6 meta variables (13 DD variables)
Labels: 	2
   * deadlock -> 1 state(s) (14 nodes)
   * init -> 1 state(s) (14 nodes)
-------------------------------------------------------------- 

Model checking property "1": P=? [F ((JohnCalls = 1) | (MaryCalls = 1))] ...
Result (for initial states): 0.9893561111
Time for model checking: 0.017s.
