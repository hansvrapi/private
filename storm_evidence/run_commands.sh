#!/bin/bash
 
declare -a StringArray=("asia" "alarm" "cancer" "andes"  "child" "earthquake" "hepar2" "insurance" "sachs" "survey")
 
# Read the array values with space
for network in "${StringArray[@]}"; do
  for d in {1,10,30}; do
  	/home/hans/Desktop/storm-bn-refactoring/build/bin/storm-bn-robin /home/hans/Desktop/private/storm_evidence/ $d $network
  done
done
