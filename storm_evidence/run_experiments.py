import os
import pandas as pd
import math
import random

networks = ['asia','alarm',  'cancer']#, 'child', 'earthquake', 'insurance', 'sachs', 'survey', 'andes', 'hepar2']
ev_nodes_percentage = [1, 10, 30]


def construct_ev_tailored_networks(): 
    
    for network in networks:
        var_file = open(f'var_files/{network}.var')
        content = var_file.readlines()
        var_number = sum(1 for line in open(f'var_files/{network}.var'))
        bif_file = open(f'bif_files/no_evidence/{network}.bif')
        bif_content = bif_file.read()
        for perc in ev_nodes_percentage:
            ev_list = []
            ev_val_list = []
            vars_to_consider = math.ceil(perc/100 * var_number)
            #create list with random numbers to select variables randomly
            var_random_list = random.sample(range(var_number-1), vars_to_consider)
            var_random_list.sort()
            for index in var_random_list:
                var_line = content[index]
                #parse var line
                ind_v = var_line.find('-')
                var_name = var_line[:ind_v].strip()    
                ev_list.append(var_name)
                end_val = var_line.find('\n')
                ev_val_numb = int(var_line[ind_v+1:end_val].strip())
                ev_val_list.append(ev_val_numb)

            ev_string = f'evidence first {{\n'
            for var in ev_list:
                ev_string += f'\t{var}=0;\n'
            ev_string += f'}}\n'
            hyp_random = random.sample(range(var_random_list[vars_to_consider-1]+1, var_number), 1)
            hyp_line = content[hyp_random[0]]
            #parse var line
            end_h = hyp_line.find('-')
            hypothesis = hyp_line[:end_h].strip()
            hyp_value = hyp_line[end_h+1:].strip()
            
            #create query
            create_query_ev(hypothesis, network, perc)
            create_query_no_ev(hypothesis, int(hyp_value), ev_list, ev_val_list, network, perc)


            hyp_string = f'hypothesis second {{\n\t{hypothesis}=0;\n}}'

            #create bif file with hypothesis and evidence
            perc_bif_file = open(f'bif_files/{perc}/{network}.bif', 'w')
            perc_bif_file.write(f'{bif_content}{ev_string}{hyp_string}')



def create_query_ev(hypothesis:str, network:str, perc:int):
    query_file = open(f'query_files/{perc}/{network}.query', 'w')
    query_str = f'storm --jani jani_files/{perc}/{network}.jani --prop "P=? [G({hypothesis}=0 | {hypothesis}=-1)]"'
    #for var in var_list:
    query_file = open(f'query_files/{perc}/{network}.query', 'w')
    query_file.write(query_str)
    query_file.close()

def create_query_no_ev(hypothesis, hyp_value, ev_list, ev_val_list, network, perc):
    query_file_1 = open(f'query_files/{perc}/no_evidence/{network}_1.query', 'w')
    query_file_2 = open(f'query_files/{perc}/no_evidence/{network}_2.query', 'w')
    query_str = f'storm --jani jani_files/no_evidence/{network}.jani --prop "P=? [F('

    for i in range (len(ev_list)):
        for val in range(1, ev_val_list[i]):
            query_str += f'{ev_list[i]}={val}|'

    query_str_2 = query_str

    for val in range (1, hyp_value):
        query_str += f'{hypothesis}={val}|'
    query_str_1 = query_str[:-1]
    query_str_2 = query_str_2[:-1]
    query_str_1 += ')]"'
    query_str_2 += ')]"'
    print(query_str_1)
    print(query_str_2)
    query_file_1.write(query_str_1)
    query_file_2.write(query_str_2)


def run_experiments():
    for network in networks:
        for perc in ev_nodes_percentage:
            query_file = open(f'query_files/{perc}/{network}.query', 'r')
            #run query on evidence tailored model
            query = query_file.read()
            os.system(f'{query} > output/{perc}/{network}_output.txt')
            #run query on evidence agnostic model
            query_file_agn1 =open(f'query_files/{perc}/no_evidence/{network}_1.query', 'r')
            query_file_agn2 =open(f'query_files/{perc}/no_evidence/{network}_2.query', 'r')

            query_agn1 = query_file_agn1.read()
            query_agn2 = query_file_agn2.read()

            os.system(f'{query_agn1} > output/{perc}/no_evidence/{network}_1_output.txt')
            os.system(f'{query_agn2} > output/{perc}/no_evidence/{network}_2_output.txt')

    
def parse_results():
    compile_time_data = pd.DataFrame(columns=['agnostic','tailored','scatterclass'])
    mtbdd_nodes_data = pd.DataFrame(columns=['agnostic','tailored','scatterclass'])
    inference_time_data = pd.DataFrame(columns=['agnostic','tailored','scatterclass'])

    for network in networks:
        for perc in ev_nodes_percentage:
            #parse evidence agnostic network
            f1 = open(f'output/{perc}/no_evidence/{network}_1_output.txt')
            f2 = open(f'output/{perc}/no_evidence/{network}_2_output.txt')
            agnostic_output_1 = f1.read()
            agnostic_output_2 = f2.read()
            #parse inference time
            begin_inf_a1 = agnostic_output_1.find('Time for model checking: ') + 25
            end_inf_a1 = agnostic_output_1.find('s.', begin_inf_a1)
            time_inf_a1 = agnostic_output_1[begin_inf_a1:end_inf_a1].strip()

            begin_inf_a2 = agnostic_output_2.find('Time for model checking: ') + 25
            end_inf_a2 = agnostic_output_2.find('s.', begin_inf_a2)
            time_inf_a2 = agnostic_output_2[begin_inf_a2:end_inf_a2].strip()
            #parse compile time
            begin_comp_a = agnostic_output_1.find('Time for model construction: ') + 29
            end_comp_a = agnostic_output_1.find('s.', begin_comp_a)
            time_comp_a = agnostic_output_1[begin_comp_a:end_comp_a].strip()
            #parse MTBDD node number
            begin_line = agnostic_output_1.find('States: ') + 8
            end_line = agnostic_output_1.find('nodes)', begin_line)
            line_a = agnostic_output_1[begin_line:end_line]
            begin_nodes_a = line_a.find('(')+1
            end_nodes_a = line_a.find(' ', begin_nodes_a)
            nodes_a = line_a[begin_nodes_a:end_nodes_a].strip()

            #parse evidence tailored network
            f1 = open(f'output/{perc}/{network}_output.txt')
            tailored_output = f1.read()
            #parse inference time
            begin_inf_t = tailored_output.find('Time for model checking: ') + 25
            end_inf_t = tailored_output.find('s.', begin_inf_t)
            time_inf_t = tailored_output[begin_inf_t:end_inf_t].strip()
            #parse compile time
            begin_comp_t = tailored_output.find('Time for model construction: ') + 29
            end_comp_t = tailored_output.find('s.', begin_comp_t)
            time_comp_t = tailored_output[begin_comp_t:end_comp_t].strip()
            #parse MTBDD node number
            begin_line = tailored_output.find('States: ') + 8
            end_line = tailored_output.find('nodes)', begin_line)
            line_t = tailored_output[begin_line:end_line]
            begin_nodes_t = line_t.find('(')+1
            end_nodes_t = line_t.find(' ', begin_nodes_t)
            nodes_t = line_t[begin_nodes_t:end_nodes_t].strip()
            flag = False
            exp = ['model checking', 'model construction', 'States']
            for e in exp:
                if not e in agnostic_output_1 or not e in agnostic_output_2:
                    flag= True
                if not e in tailored_output: 
                    flag= True
            if not flag:
                df1 = pd.DataFrame([[float(time_inf_a1) + float(time_inf_a2), time_inf_t, f'qual{perc}']],columns=['agnostic','tailored','scatterclass'])    
                inference_time_data = pd.concat([inference_time_data,df1])  
                df2 = pd.DataFrame([[nodes_a, nodes_t, f'qual{perc}']],columns=['agnostic','tailored','scatterclass'])    
                mtbdd_nodes_data = pd.concat([mtbdd_nodes_data,df2])  
                df3 = pd.DataFrame([[time_comp_a, time_comp_t, f'qual{perc}']],columns=['agnostic','tailored','scatterclass'])    
                compile_time_data = pd.concat([compile_time_data,df3])  

    compile_time_data.to_csv('compile_time.csv', index=False)
    inference_time_data.to_csv('inference_time.csv', index=False)
    mtbdd_nodes_data.to_csv('mtbdd_nodes.csv', index=False)


if __name__ == "__main__":
        #construct_ev_tailored_networks()
        #os.system(f'./run_commands.sh')
        #run_experiments()
        parse_results()
        os.system('pdflatex compile_time.tex')        
        os.system('pdflatex inference_time.tex')
        os.system('pdflatex mtbdd_nodes.tex')
        os.system('rm compile_time.aux')
        os.system('rm inference_time.aux')
        os.system('rm mtbdd_nodes.aux')
        os.system('rm compile_time.log')
        os.system('rm inference_time.log')
        os.system('rm mtbdd_nodes.log')
