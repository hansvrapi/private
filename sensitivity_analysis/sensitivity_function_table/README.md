## make_tablle.py
	- python script that runs all benchmarks p* shown in the paper and gives a table containing information about the computed sensitivity functions as output.
	- Command: python3 make_table.py

## storm_pars.sh
	- shell script that is used in make_table to run all the benchmarks using storm
	
## solution_function_computation.csv
	- csv file created from parsing the data
