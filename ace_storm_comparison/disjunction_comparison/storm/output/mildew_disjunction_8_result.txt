Storm 1.6.4

Date: Tue Apr 26 11:03:49 2022
Command line arguments: --jani storm/mildew/mildew.jani --prop 'P=? [F((nedboer_1=1|mikro_1=0|middel_1=1|meldug_2=1|middel_2=0|straaling_2=0|dm_1=1|dm_3=1))]'
Current working directory: /experiments/ace_storm_comparison/disjunction_comparison

Time for model input parsing: 14.095s.

Time for model construction: 17.905s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	387942
Transitions: 	1477429
Reward Models:  none
State Labels: 	3 labels
   * init -> 1 item(s)
   * deadlock -> 86 item(s)
   * ((((((((nedboer_1 = 1) | (mikro_1 = 0)) | (middel_1 = 1)) | (meldug_2 = 1)) | (middel_2 = 0)) | (straaling_2 = 0)) | (dm_1 = 1)) | (dm_3 = 1)) -> 19343 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [F ((((((((nedboer_1 = 1) | (mikro_1 = 0)) | (middel_1 = 1)) | (meldug_2 = 1)) | (middel_2 = 0)) | (straaling_2 = 0)) | (dm_1 = 1)) | (dm_3 = 1))] ...
Result (for initial states): 0.733763678
Time for model checking: 0.164s.
