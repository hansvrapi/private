#include <iostream>
#include "storm-config.h"
#include "storm-bn-robin/src/parser/BNNetwork.h"



#include <string>
#include "storm-bn-robin/src/jani/JaniFileCreator.h"
#include "storm-bn-robin/src/QueriesCreator.h"
#include "storm-bn-robin/src/VariablesFileCreator.h"



#include <time.h>
#include <algorithm>

#include "storm/utility/initialize.h"
#include "storm-cli-utilities/cli.h"
#include "storm/settings/modules/GeneralSettings.h"
#include "storm/exceptions/FileIoException.h"


#include "storm/storage/jani/JSONExporter.h"
#include "storm/storage/expressions/Expressions.h"
#include "storm/storage/expressions/ExpressionManager.h"
#include "storm-parsers/parser/ExpressionParser.h"
#include <sstream>

using namespace std::chrono;


using namespace std;

int main(const int argc, const char **argv) {

    try {


        storm::utility::setUp();
        storm::cli::printHeader("Storm-bn-robin", argc, argv);
        Utils util;


        std::string variableFile;
        bool findOrdering = false;
        if(argc < 4){
            std::cout << "Arguments are missing!";
            return 1;
        }
        std::string folder  = argv[1];
        std::string ev_nodes = argv[2];
        std::string fileName = argv[3];
        //while ("end" != fileName) {
        if (findOrdering) {
            variableFile.clear();
        } else {
            variableFile = folder + "var_files/" + fileName + ".var";
            std::cout << variableFile;
        }
        BNNetwork network(folder, fileName, ".bif", ev_nodes, variableFile);
        std::string fileContent = VariablesFileCreator::createVariableFileContent(network.getSortedProbabilityTables());
        if (!fileContent.empty()) {
            //util.writeToFile(fileContent, folder + fileName + ".var");
            //QueriesCreator qCreator;
            //util.writeToFile(qCreator.createQueries(fileName, fileContent), folder + fileName + ".query");
            //std::cout << qCreator.createMARQuery();
            JaniFileCreator creator(network);
            util.writeToFile(creator.create(), folder + "jani_files/" + ev_nodes + "/" + fileName + ".jani");
        }
        std::cout << "done." << "\n";
        return 0;

        //}

            // std::string fileName = "alarmev";
            // auto start0 = high_resolution_clock::now();

            // BNNetwork bnnetwork(STORM_TEST_RESOURCES_DIR "/bn/", fileName, ".bif");
        }

        catch (storm::exceptions::BaseException const&exception) {
            STORM_LOG_ERROR(
                    "An exception caused Storm to terminate. The message of the exception is: " << exception.what());
            return 1;
        } catch (std::exception const&exception) {
            STORM_LOG_ERROR(
                    "An unexpected exception occurred and caused Storm to terminate. The message of this exception is: "
                            << exception.what());
            return 2;
        }
    }


