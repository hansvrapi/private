//
// Created by Bahare Salmani on 2019-04-25.
//

#ifndef BNPARSER_PROBABILITYTABLE_H
#define BNPARSER_PROBABILITYTABLE_H

#include <vector>
#include <string>
#include "BIFFORMAT.h"
#include "Utils.h"
#include "ProbabilityRow.h"
#include "BNNode.h"



class ProbabilityTable {
public:
    ProbabilityTable(){}
    ProbabilityTable(std::string BIFdeclaration, std::map<std::string, BNNode> nodes);
    ProbabilityTable(BNNode node, std::vector<BNNode> parents, std::vector<ProbabilityRow> rows);
    std::string getNodeName();
    std::vector<std::string> getParentsNames();
    std::vector<ProbabilityRow> getRows();
    std::vector<std::string> getPossibleValues();
    BNNode getNode();
    std::vector<BNNode> getParents();
    bool isTheNodeObserved();
    bool isTheNodeQuestioned();

private:
    std::string theNodeName;
    BNNode node;
    std::vector<std::string> parentsNames;
    std::vector<BNNode> parents;
    std::map<std::string, BNNode> allNamesToNodes;
    void build();
    std::vector<ProbabilityRow> probabilityEntries;
    std::string tableDeclaration;
    bool isIndependent;
    void parseNodes();
    void parseRows();

};


#endif //BNPARSER_PROBABILITYTABLE_H
